#!/usr/bin/env bash

# Main env variables
export APP_ENV=${APP_ENV:-local}
export APP_PORT=${APP_PORT:-80}
export DB_PORT=${DB_PORT:-3306}
export DB_ROOT_PASS=${DB_ROOT_PASS:-secret}
export DB_NAME=${DB_NAME:-homestead}
export DB_USER=${DB_USER:-homestead}
export DB_PASS=${DB_PASS:-secret}
export WORK_DIR=${WORK_DIR:-/var/www/html/app}
export SRC_VOLUME=${SRC_VOLUME:-./application}
export SSH_PRIVATE_KEY=${SSH_PRIVATE_KEY}
export VIRTUAL_HOST=${VIRTUAL_HOST}


# Check machine and add XDEBUG_HOST
UNAMEOUT="$(uname -s)"
case "${UNAMEOUT}" in
    Linux*)             MACHINE=linux;;
    Darwin*)            MACHINE=mac;;
    MINGW64_NT-10.0*)   MACHINE=mingw64;;
    *)                  MACHINE="UNKNOWN"
esac

if [ "$MACHINE" == "UNKNOWN" ]; then
    echo "Unsupported system type"
    echo "System must be a Macintosh, Linux or Windows"
    echo ""
    echo "System detection determined via uname command"
    echo "If the following is empty, could not find uname command: $(which uname)"
    echo "Your reported uname is: $(uname -s)"
fi

# Set environment variables for dev
if [ "$MACHINE" == "linux" ]; then
    if grep -q Microsoft /proc/version; then # WSL
        export XDEBUG_HOST=10.0.75.1
    else
        if [ "$(command -v ip)" ]; then
            export XDEBUG_HOST=$(ip addr show docker0 | grep "inet\b" | awk '{print $2}' | cut -d/ -f1)
        elif [ "$(command -v ifconfig)" ]; then
            export XDEBUG_HOST=$(ifconfig docker0 | grep "inet addr" | cut -d ':' -f 2 | cut -d ' ' -f 1)
        else
            export XDEBUG_HOST=127.0.0.1
        fi
    fi
    SEDCMD="sed -i"
elif [ "$MACHINE" == "mac" ]; then
    export XDEBUG_HOST=$(ipconfig getifaddr en0) # Ethernet

    if [ -z "$XDEBUG_HOST" ]; then
        export XDEBUG_HOST=$(ipconfig getifaddr en1) # Wifi
    fi
    SEDCMD="sed -i .bak"
elif [ "$MACHINE" == "mingw64" ]; then # Git Bash
    export XDEBUG_HOST=10.0.75.1
    SEDCMD="sed -i"
fi


# Linux: Get host address
# Via: http://stackoverflow.com/questions/13322485/how-to-i-get-the-primary-ip-address-of-the-local-machine-on-linux-and-os-x
#export XDEBUG_HOST=$(ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1' | head -n1)

# Decide which docker-compose file to use
COMPOSE_FILE="dev"

# Disable pseudo-TTY allocation for CI (Jenkins)
TTY="-T"

if [ ! -z "$BUILD_NUMBER" ]; then
    COMPOSE_FILE="ci"
    TTY="-T"
elif [  "$APP_ENV" == "prod" ] ||  [  "$APP_ENV" == "production" ] ; then
    COMPOSE_FILE="prod"
    TTY="-T"
fi

COMPOSE="docker-compose -f docker-compose.$COMPOSE_FILE.yml"


# Is the environment running
PSRESULT="$($COMPOSE ps -q)"
if [ ! -z "$PSRESULT" ]; then
    EXEC="yes"
else
    EXEC="no"
fi

if [ $# -gt 0 ];then
	
	if [ "$1" == "start" ]; then
        $COMPOSE up -d
		
    elif [ "$1" == "stop" ]; then
        $COMPOSE down
	
    elif [ "$1" == "artisan" ] || [ "$1" == "art" ]; then
        shift 1
        $COMPOSE run --rm $TTY \
            app \
            php artisan "$@"
    
	elif [ "$1" == "composer" ] || [ "$1" == "comp" ]; then
        shift 1
        $COMPOSE run --rm $TTY \
            app \
            composer "$@"
	
	elif [ "$1" == "run" ] ; then
        shift 1
        $COMPOSE run --rm $TTY \
            app \
            "$@"

    
    elif [ "$1" == "test" ]; then
        shift 1
        $COMPOSE run --rm $TTY \
            -w /var/www/html/app \
            app \
            ./vendor/bin/phpunit "$@"
	
	elif [ "$1" == "git" ] ; then
        shift 1
        $COMPOSE run --rm $TTY \
            app \
            git "$@"
    
	elif [ "$1" == "t" ]; then
		shift 1
		$COMPOSE run --rm $TTY \
			app \
			sh -c "cd /var/www/html/app && ./vendor/bin/phpunit $@"

    # If "npm" is used, run npm
    # from our node container
    elif [ "$1" == "npm" ]; then
        shift 1
        $COMPOSE run --rm $TTY \
            -w /var/www/html/app \
            node \
            npm "$@"

    # If "gulp" is used, run gulp
    # from our node container
    elif [ "$1" == "gulp" ]; then
        shift 1
        $COMPOSE run --rm $TTY \
            -w /var/www/html/app \
            node \
            ./node_modules/.bin/gulp "$@"
    else
        $COMPOSE "$@"
    fi
else
    $COMPOSE ps
fi
